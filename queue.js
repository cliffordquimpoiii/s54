
let collection = [];

// Write the queue functions below.
function print(){
    return collection;
}

function enqueue(element){
    if(collection.length == 0){
        collection[0] = element;
    } else {
        collection[collection.length] = element;
    }
    return collection;
}

function dequeue(){
    let element = [];
    for(let i = 0; i < collection.length - 1; i++){
        element[i] = collection[i+1];
    }
    return collection = [...new Set(element)];
}

function front(){
    return collection[0];
}

function size(){
    return collection.length;
}

function isEmpty(){
    if(collection.length > 0){
        return false;
    } else {
        return true;
    }
}



module.exports = { 
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty  
};